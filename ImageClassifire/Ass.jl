### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ ed4544f8-c161-48eb-b8d1-50a076eea198
using Flux

# ╔═╡ 3fb3aae4-e06d-4b5f-92a0-94e7a6b56796
using Flux.Data: DataLoader

# ╔═╡ 9bb9c224-65f3-4c09-94e3-4a4a332b50e6
using Flux.Optimise: Optimiser, WeightDecay

# ╔═╡ 39e73e1c-14e5-452d-9245-6636c043badb
using Flux: onehotbatch, onecold

# ╔═╡ f4f9c44c-7325-4220-994a-8ebe4e2eda55
using Flux.Losses: logitcrossentropy

# ╔═╡ 16b31ff1-3f18-4663-8a7a-8694dd7f6639
using Statistics, Random

# ╔═╡ 624c662e-6e71-4409-abbf-1f4aa9dcb535
using Logging: with_logger

# ╔═╡ 394a0c34-d3f5-4a46-92fb-549d0d0ef695
using TensorBoardLogger: TBLogger, tb_overwrite, set_step!, set_step_increment!

# ╔═╡ 557ae944-6162-4577-b2ed-1152553e4bc6
using ProgressMeter: @showprogress

# ╔═╡ 6d02fb24-d9e3-4a08-a9ae-6092f9fb2a70
using MLDatasets 

# ╔═╡ f673b758-15ed-4ac3-bdf7-4dcba2b94da7
using CUDA

# ╔═╡ ae887e9b-b3b9-483d-b252-acf921144096
using Images

# ╔═╡ 9c74e74c-9dae-40c7-a8e2-d15ad4bee9a1
using CSV

# ╔═╡ 301f87fd-9a74-4f9a-8432-f881692c9071
using PlutoUI

# ╔═╡ f8d63551-38a0-4171-bf84-15c0ce84554f

# begin 
# 	using Pkg
# 	Pkg.activate("project.toml")
#     Pkg.add("BSON")
# 	Pkg.add("CUDA")
# 	Pkg.add("Flux")
# 	Pkg.add("Logging")
# 	Pkg.add("TensorBoardLogger")
# 	using Flux
# 	# using Flux.Data: DataLoader
# 	# 
# 	# using Flux: onehotbatch, onecold
# 	# 
# 	using Statistics, Random 
# 	using Logging: with_logger
# end

# ╔═╡ af229aff-32d0-48ce-8fcf-9af23026a8b6
import BSON

# ╔═╡ 7e5414b9-e0d3-4c03-b97e-007f5c054b84
# using DataFrames

# ╔═╡ 233a35ee-2e25-40b0-914e-27ad8a004353
function LeNet5(; imgsize=(28,28,1), nclasses=10) 
    out_conv_size = (imgsize[1]÷4 - 3, imgsize[2]÷4 - 3, 16)
    
    return Chain(
            Conv((5, 5), imgsize[end]=>6, relu),
            MaxPool((2, 2)),
            Conv((5, 5), 6=>16, relu),
            MaxPool((2, 2)),
            flatten,
            Dense(prod(out_conv_size), 120, relu), 
            Dense(120, 84, relu), 
            Dense(84, nclasses)
          )
end 

# ╔═╡ f7bf1fe5-6436-4d84-a5ec-7919f337a4a2
# function read_data(typeData, labelsInfo, imageSize, path)
#     #Intialize x matrix
#     # x = Array{Image{Colors.Gray{Float32}}}(size(labelsInfo,1))

# 	 xtrain = zeros(size(labelsInfo, 1), imageSize)
	
	
# 	ID ="ID"
#     for (index, idImage) in enumerate(labelsInfo.ID)
		
#         #Read image file
#         nameFile = "$(path)/$(typeData)Resized/$(idImage).Bmp"
#         img = load(nameFile)
		
		
#         #Convert img to float values
# 		  temp = Float32(img)
#         #Convert color images to gray images
#         # img  = convert(Image{Colors.Gray{Float32}},img)
# 		 # t 
# 		 #   if ndims(float32(img)) == 3
# 		 # t = mean(float32(img).data, 1)
# 		 # end
		
#   		xtrain[index,:] = reshape(temp, )
		
#         # x[index] = img

#     end
#     return xtrain
# end

# ╔═╡ 7efa3695-c0eb-473b-abc2-156021b801ee
# #typeData could be either "train" or "test.
# #labelsInfo should contain the IDs of each image to be read
# #The images in the trainResized and testResized data files
# #are 20x20 pixels, so imageSize is set to 400.
# #path should be set to the location of the data files.

# function read_data(typeData, labelsInfo, imageSize, path)
#  #Intialize x matrix
#  x = zeros(size(labelsInfo, 1), imageSize)

#  for (index, idImage) in enumerate(labelsInfo[1,"ID"]) 
#   #Read image file 
#   nameFile = "$(path)/$(typeData)Resized/$(idImage).Bmp"
#   img = load(nameFile)

#   #Convert img to float values 
#   temp = float32(img)

#   #Convert color images to gray images
#   #by taking the average of the color scales. 
#   if ndims(temp) == 3
#    temp = mean(temp.data, 1)
#   end
    
#   #Transform image matrix to a vector and store 
#   #it in data matrix 
#   # x[index, :] = reshape(temp, 1, imageSize)
#  end 
#  return x
# end

# ╔═╡ 5d1ee6aa-6a3f-46c0-b3b7-1870137c48ba
# begin
# 	x = load("Data/trainResized/1.Bmp")
# 	x1 = load("Data/trainResized/2.Bmp")
# 	x2 = load("Data/trainResized/3.Bmp")
# 	x3 = load("Data/trainResized/4.Bmp")
# 	x4 = load("Data/trainResized/5.Bmp")
	
	
	
# 	temp  = float32(x)
# 	temp1 = float32(x1)
# 	temp2 = float32(x2)
# 	temp3 = float32(x3)
# 	temp4 = float32(x4)
	
# 	if ndims(temp) == 3
# 	temp = mean(temp.data, 1)
# 	end
	
	
# 		if ndims(temp1) == 3
# 	temp1 = mean(temp1.data, 1)
# 	end
	
# 		if ndims(temp2) == 3
# 	temp2 = mean(temp2.data, 1)
# 	end
	
# 		if ndims(temp3) == 3
# 	temp3 = mean(temp3.data, 1)
# 	end
	
# 		if ndims(temp4) == 3
# 	temp4 = mean(tem4.data, 1)
# 	end
	
	
# 	imageSize = 400
	
# 	temp = reshape(temp, 1, imageSize)
# 	temp1 = reshape(temp1, 1, imageSize)
# 	temp2 = reshape(temp2, 1, imageSize)
# 	temp3 = reshape(temp3, 1, imageSize)
# 	temp4 = reshape(temp4, 1, imageSize)
		
		
# 		x[1, :] = reshape(temp, 1, imageSize)
# 		x[2, :] = reshape(temp1, 1, imageSize)
# 		x[3, :] = reshape(temp2, 1, imageSize)
# 		x[4, :] = reshape(temp3, 1, imageSize)
# 		x[5, :] = reshape(temp4, 1, imageSize)
	
# 	with_terminal() do
# 		println(x)
# 	end
# end
# begin
#     # xtrain, ytrain = MLDatasets.MNIST.traindata(Float32)
#     xtest, ytest = MNIST.testdata(dir = "Data/trainResized")

# 	with_terminal() do
# 		# println(xtest)
# 	end
    # xtrain = reshape(xtrain, 28, 28, 1, :)
    # xtest = reshape(xtest, 28, 28, 1, :)

    # ytrain, ytest = onehotbatch(ytrain, ["pneumonia","normal"]), onehotbatch(ytest, ["pneumonia","normal"])

    # train_loader = DataLoader((xtrain, ytrain), batchsize=args.batchsize, shuffle=true)
    # test_loader = DataLoader((xtest, ytest),  batchsize=args.batchsize)
# end


# ╔═╡ 5a8a7f34-e731-471e-9b59-babb76d7753e


# ╔═╡ f0e3ca65-cf9a-4ee1-9f44-56485c807b00


# ╔═╡ 19616b9c-0a64-4172-a3e7-ff8ec47a7023
# global yData = []

# ╔═╡ ffb87a95-6724-4a17-880e-83b49d66d982
# function loadData()
# imageSize = 400 # 20 x 20 pixel

# #Set location of data files, folders
# path = "Data"

# 	# re_df= CSV.read("Real estate.csv", DataFrame)
	
# 	#Read information about training data , IDs.
# 	# labelsInfoTrain = CSV.File("$(path)/trainLabels.csv")
# 	labelsInfoTrain = CSV.read("$(path)/trainLabels.csv",DataFrame )

# #Read training matrix
# xTrain = read_data("train", labelsInfoTrain, imageSize, path)
#  xData = xTrain
# #Read information about test data ( IDs ).
# labelsInfoTest = CSV.File("$(path)/sampleSubmission.csv")
# 	labelsInfoTest = CSV.File("$(path)/sampleSubmission.csv")

# #Read test matrix
# xTest = read_data("test", labelsInfoTest, imageSize, path)
# 	# yData = xTest
# end

# ╔═╡ 1f96eab0-3717-4062-b8ca-cc486383929a
# loadData()

# ╔═╡ 0e153b4e-506e-497a-8056-432d110e92e7
with_terminal() do
	# println(xData)
end

# ╔═╡ 8a7651c8-7945-4f6f-9dfc-9e8d2ba75810
labels = ["normal","pneumonia"]

# ╔═╡ 50081063-2ebe-4a22-85aa-1927e254e8a4
function get_data(args)
	
	
# 	#load Data
# 	imageSize = 400 # 20 x 20 pixel

# #Set location of data files, folders
# path = "Data"

# #Read information about training data , IDs.
# labelsInfoTrain = readtable("$(path)/trainLabels.csv")

# #Read training matrix
# xTrain = read_data("train", labelsInfoTrain, imageSize, path)

# #Read information about test data ( IDs ).
# labelsInfoTest = readtable("$(path)/sampleSubmission.csv")

# #Read test matrix
# xTest = read_data("test", labelsInfoTest, imageSize, path)
	
	
	
	
    # xtrain, ytrain = MLDatasets.MNIST.traindata(Float32)
	xtrain, ytrain = MNIST.traindata(Float32, dir="./MNIST")
	xtest, ytest = MNIST.testdata(Float32, dir="./MNIST")

    # xtest, ytest = MLDatasets.MNIST.testdata(Float32)

    xtrain = reshape(xtrain, 28, 28, 1, :)
    xtest = reshape(xtest, 28, 28, 1, :)
	labels = 0:9
    ytrain, ytest = onehotbatch(ytrain, labels), onehotbatch(ytest,labels)

    train_loader = DataLoader((xtrain, ytrain), batchsize=args.batchsize, shuffle=true)
    test_loader = DataLoader((xtest, ytest),  batchsize=args.batchsize)
    
    return train_loader, test_loader
end

# ╔═╡ 488a4162-2f3b-48ae-ad04-0fd6b8035f52
loss(ŷ, y) = logitcrossentropy(ŷ, y)

# ╔═╡ da696be4-59f2-4508-b733-65d224bfbd3e
num_params(model) = sum(length, Flux.params(model)) 

# ╔═╡ 45513dcb-6407-4d29-8ad8-08183d331165
round4(x) = round(x, digits=4)

# ╔═╡ 71a4c2d3-3579-4d35-95c9-4369481ad135
function eval_loss_accuracy(loader, model, device)
    l = 0f0
    acc = 0
    ntot = 0
    for (x, y) in loader
        x, y = x |> device, y |> device
        ŷ = model(x)
        l += loss(ŷ, y) * size(x)[end]        
        acc += sum(onecold(ŷ |> cpu) .== onecold(y |> cpu))
        ntot += size(x)[end]
    end
    return (loss = l/ntot |> round4, acc = acc/ntot*100 |> round4)
end

# ╔═╡ fd87c578-399b-4023-b952-f680f1741ed3
Base.@kwdef mutable struct Args
    η = 3e-4             # learning rate
    λ = 0                # L2 regularizer param, implemented as weight decay
    batchsize = 128      # batch size
    epochs = 10          # number of epochs
    seed = 0             # set seed > 0 for reproducibility
    use_cuda = true      # if true use cuda (if available)
    infotime = 1 	     # report every `infotime` epochs
    checktime = 5        # Save the model every `checktime` epochs. Set to 0 for no checkpoints.
    tblogger = true      # log training with tensorboard
    savepath = "runs/"    # results path
end

# ╔═╡ 20835755-82b0-4e69-8818-0abb60904638
function train(; kws...)
    args = Args(; kws...)
    args.seed > 0 && Random.seed!(args.seed)
    use_cuda = args.use_cuda && CUDA.functional()
    
    if use_cuda
        device = gpu
        @info "Training on GPU"
    else
        device = cpu
        @info "Training on CPU"
    end

    ## DATA
    train_loader, test_loader = get_data(args)
    @info "Custom MNIST Dataset Loaded"

    ## MODEL AND OPTIMIZER
    model = LeNet5() |> device
    @info "~ LeNet5 model ~"    
    
    ps = Flux.params(model)  

    opt = ADAM(args.η) 
    if args.λ > 0 # add weight decay, equivalent to L2 regularization
        opt = Optimiser(WeightDecay(args.λ), opt)
    end
    
    ## LOGGING UTILITIES
    if args.tblogger 
        tblogger = TBLogger(args.savepath, tb_overwrite)
        set_step_increment!(tblogger, 0) # 0 auto increment since we manually set_step!
        @info "log File => \"$(args.savepath)\""
    end
    
    function report(epoch)
        train = eval_loss_accuracy(train_loader, model, device)
        test = eval_loss_accuracy(test_loader, model, device)        
        println("Epoch: $epoch   Train: $(train)   Test: $(test)")
        if args.tblogger
            set_step!(tblogger, epoch)
            with_logger(tblogger) do
                @info "train" loss=train.loss  acc=train.acc
                @info "test"  loss=test.loss   acc=test.acc
            end
        end
    end
    
    ## TRAINING
    @info "Start Training"
    report(0)
    for epoch in 1:args.epochs
        @showprogress for (x, y) in train_loader
            x, y = x |> device, y |> device
            gs = Flux.gradient(ps) do
                    ŷ = model(x)
                    loss(ŷ, y)
                end

            Flux.Optimise.update!(opt, ps, gs)
        end
        
        ## Printing and logging
        epoch % args.infotime == 0 && report(epoch)
        if args.checktime > 0 && epoch % args.checktime == 0
            !ispath(args.savepath) && mkpath(args.savepath)
            modelpath = joinpath(args.savepath, "model.bson") 
            let model = cpu(model) #return model to cpu before serialization
                BSON.@save modelpath model epoch
            end
            @info "Model saved in \"$(modelpath)\""
        end
    end
end

# ╔═╡ 42427bf1-e8cb-4287-968f-1aaadf4a899d
train()

# ╔═╡ Cell order:
# ╟─f8d63551-38a0-4171-bf84-15c0ce84554f
# ╠═ed4544f8-c161-48eb-b8d1-50a076eea198
# ╠═3fb3aae4-e06d-4b5f-92a0-94e7a6b56796
# ╠═9bb9c224-65f3-4c09-94e3-4a4a332b50e6
# ╠═39e73e1c-14e5-452d-9245-6636c043badb
# ╠═f4f9c44c-7325-4220-994a-8ebe4e2eda55
# ╠═16b31ff1-3f18-4663-8a7a-8694dd7f6639
# ╠═624c662e-6e71-4409-abbf-1f4aa9dcb535
# ╠═394a0c34-d3f5-4a46-92fb-549d0d0ef695
# ╠═557ae944-6162-4577-b2ed-1152553e4bc6
# ╠═6d02fb24-d9e3-4a08-a9ae-6092f9fb2a70
# ╠═af229aff-32d0-48ce-8fcf-9af23026a8b6
# ╠═f673b758-15ed-4ac3-bdf7-4dcba2b94da7
# ╠═ae887e9b-b3b9-483d-b252-acf921144096
# ╠═7e5414b9-e0d3-4c03-b97e-007f5c054b84
# ╠═9c74e74c-9dae-40c7-a8e2-d15ad4bee9a1
# ╠═301f87fd-9a74-4f9a-8432-f881692c9071
# ╠═233a35ee-2e25-40b0-914e-27ad8a004353
# ╟─f7bf1fe5-6436-4d84-a5ec-7919f337a4a2
# ╟─7efa3695-c0eb-473b-abc2-156021b801ee
# ╟─5d1ee6aa-6a3f-46c0-b3b7-1870137c48ba
# ╠═5a8a7f34-e731-471e-9b59-babb76d7753e
# ╠═f0e3ca65-cf9a-4ee1-9f44-56485c807b00
# ╠═19616b9c-0a64-4172-a3e7-ff8ec47a7023
# ╟─ffb87a95-6724-4a17-880e-83b49d66d982
# ╠═1f96eab0-3717-4062-b8ca-cc486383929a
# ╠═0e153b4e-506e-497a-8056-432d110e92e7
# ╠═8a7651c8-7945-4f6f-9dfc-9e8d2ba75810
# ╠═50081063-2ebe-4a22-85aa-1927e254e8a4
# ╠═488a4162-2f3b-48ae-ad04-0fd6b8035f52
# ╠═71a4c2d3-3579-4d35-95c9-4369481ad135
# ╠═da696be4-59f2-4508-b733-65d224bfbd3e
# ╠═45513dcb-6407-4d29-8ad8-08183d331165
# ╠═fd87c578-399b-4023-b952-f680f1741ed3
# ╠═20835755-82b0-4e69-8818-0abb60904638
# ╠═42427bf1-e8cb-4287-968f-1aaadf4a899d
